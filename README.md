SOAL SOAL UTS PRAK PBO
# No.1
Mampu mendemonstrasikan penyelesaian masalah dengan pendekatan matematika dan algoritma pemrograman secara tepat. (Lampirkan link source code terkait)
Jawab :
contoh blok kode yang relevan untuk menjelaskan bagaimana operasi matematika dan aritmatika digunakan dalam kelas TiketKereta:
```
.java
   public void isiSaldo(UserDatabase user) {
        Scanner input = new Scanner(System.in);
        System.out.println();
        System.out.println("\t[1] Lanjut\n\t[2] Kembali ke Menu");
        System.out.println();
        System.out.print("Pilihan Anda : ");
        String tanya = input.nextLine();
        if (tanya.equals("1")) {
            System.out.print("\tMasukkan nominal uang : Rp. ");
            int saldo = Integer.parseInt(input.nextLine());
            // user.getUser(0).add(String.valueOf(saldo));
            double currentSaldo = Double.parseDouble(user.getUser(0).get(3));
            double saldoToAdd = Double.parseDouble(user.getUser(0).get(3));

            user.getUser(0).set(3, String.valueOf(currentSaldo + saldo));
            // user.getUser(0).remove(4);
            System.out.println();
            System.out.println("\tSaldo telah di isi");
            System.out.println();

        }
        System.out.println("Tekan ENTER untuk kembali ke Menu");
        input.nextLine();
    }

`````
Pada blok kode di atas, terdapat operasi matematika dan aritmatika yang digunakan untuk menghitung Saldo pengguna.
Kode yang Anda berikan adalah bagian dari sebuah program yang tampaknya berfungsi untuk mengisi saldo dalam database pengguna (user). Saya akan menjelaskan secara garis besar apa yang dilakukan oleh kode tersebut.
1. Program meminta input dari pengguna untuk memilih opsi: melanjutkan (1) atau kembali ke menu (2).
2. Jika pengguna memilih "1" (melanjutkan), program akan meminta pengguna untuk memasukkan nominal uang yang akan ditambahkan ke saldo.
3. Nominal uang yang dimasukkan oleh pengguna diambil sebagai string dan diubah menjadi bilangan bulat (integer) menggunakan metode `parseInt()` dari kelas `Integer`.
4. Program mengambil saldo saat ini dari pengguna pertama dalam database pengguna (`user.getUser(0)`), yang didapatkan dari indeks 3 dalam daftar yang mewakili pengguna tersebut.
5. Saldo saat ini dan nominal uang yang dimasukkan oleh pengguna diubah menjadi tipe data `double` menggunakan metode `parseDouble()` dari kelas `Double`.
6. Saldo saat ini ditambahkan dengan nominal uang yang dimasukkan oleh pengguna dan hasilnya disimpan kembali dalam indeks 3 dari daftar pengguna pertama.
7. Setelah itu, pesan "Saldo telah di isi" akan ditampilkan ke layar.
8. Program kemudian menunggu pengguna menekan tombol "ENTER" untuk kembali ke menu.


# No.2
Mampu menjelaskan algoritma dari solusi yang dibuat (Lampirkan link source code terkait)
Jawab :

**Algoritma Pemesanan**
![Algoritma Pemesanan](ss/3. Algoritma Pemesanan Tiket.png)
**Proses Mengakses Pemesanan**
![Algoritma Pemesanan](ss/1. Algoritma_Pemesanan.gif)

![Algoritma Pemesanan](src/TiketKereta/Pesanan.java)

Algoritma pemesanan tiket kereta api adalah serangkaian langkah yang diikuti untuk melakukan proses pemesanan tiket kereta. Berikut adalah penjelasan langkah-langkahnya:

1. Tampilkan pesan selamat datang kepada pengguna.
   - Langkah ini bertujuan untuk memberi salam kepada pengguna saat mereka memulai proses pemesanan tiket.

2. Tampilkan pilihan menu kepada pengguna.
   - Langkah ini menampilkan opsi menu yang tersedia, seperti "Pencarian jadwal kereta", "Pemesanan tiket", "Pembatalan tiket", "Lihat riwayat pemesanan", dan "Keluar".

3. Dalam loop while, periksa apakah pengguna telah memilih opsi "Pemesanan tiket".
   - Jika pengguna memilih opsi "Pemesanan tiket", lanjutkan ke langkah berikutnya. Jika tidak, kembali ke langkah 2.

4. Tampilkan formulir pemesanan tiket.
   - Langkah ini menampilkan formulir yang meminta pengguna untuk memasukkan informasi seperti stasiun asal, stasiun tujuan, tanggal keberangkatan, dan jumlah penumpang.

5. Terima input dari pengguna.
   - Dalam langkah ini, sistem menerima input dari pengguna berdasarkan formulir pemesanan tiket.

6. Verifikasi ketersediaan tiket untuk perjalanan yang diminta.
   - Sistem memeriksa ketersediaan tiket berdasarkan informasi yang dimasukkan oleh pengguna, seperti stasiun asal, stasiun tujuan, dan tanggal keberangkatan.

7. Jika tiket tersedia, tampilkan rincian tiket kepada pengguna dan tanyakan konfirmasi pembayaran.
   - Jika tiket tersedia, sistem akan menampilkan rincian tiket kepada pengguna, seperti nomor kereta, jadwal keberangkatan, dan harga tiket. Selanjutnya, sistem akan meminta pengguna untuk mengonfirmasi pembayaran.

8. Jika pengguna mengkonfirmasi pembayaran, lakukan proses pembayaran dan tampilkan pesan sukses pemesanan tiket.
   - Jika pengguna mengonfirmasi pembayaran, sistem akan memproses pembayaran sesuai dengan metode yang telah ditentukan dan menampilkan pesan sukses pemesanan tiket.

9. Jika tiket tidak tersedia, tampilkan pesan bahwa tiket tidak dapat dipesan.
   - Jika tiket tidak tersedia untuk perjalanan yang diminta, sistem akan memberi tahu pengguna bahwa tiket tidak dapat dipesan.

10. Kembali ke langkah 2.
    - Setelah langkah-langkah pemesanan tiket selesai, sistem akan kembali ke langkah 2 untuk menampilkan menu pilihan kepada pengguna.

11. Tampilkan pesan selamat tinggal.
    - Setelah pengguna memilih opsi "Keluar", sistem akan menampilkan pesan selamat tinggal kepada pengguna.

Ini adalah penjelasan tentang algoritma pemesanan tiket kereta api yang mencakup langkah-langkah umum yang harus diikuti dalam proses pemesanan tiket..



# No.3
Mampu menjelaskan konsep dasar OOP

Jawab:
- Abstraksi (Abstraction): Abstraksi dalam pemrograman berorientasi objek adalah konsep di mana Anda memperlihatkan fungsi utama dari sebuah kelas kepada publik, tetapi menyembunyikan detail pelaksanaannya. Ini memungkinkan pengguna kelas untuk menggunakan fungsionalitas yang disediakan tanpa perlu tahu bagaimana hal tersebut diimplementasikan secara internal. Misalnya, Anda dapat memiliki kelas "Mobil" yang memiliki metode "maju" dan "mundur". Pengguna kelas hanya perlu tahu cara menggunakan metode-metode ini tanpa perlu tahu bagaimana mobil sebenarnya bergerak.
 
- Enkapsulasi (Encapsulation): Enkapsulasi adalah konsep di mana Anda membatasi akses langsung ke data atau metode di dalam kelas dan mengatur akses tersebut melalui modifier akses (misalnya, public, private, protected). Dengan enkapsulasi, Anda dapat membatasi perubahan langsung pada data kelas dan memastikan bahwa perubahan tersebut hanya dapat dilakukan melalui metode-metode yang sesuai. Misalnya, Anda dapat menggunakan metode "setNama" dan "getNama" untuk mengatur dan mengakses data "nama" dalam kelas "Mahasiswa", sehingga Anda dapat menerapkan validasi atau logika lainnya saat mengakses data tersebut.
 
- Pewarisan (Inheritance): Pewarisan adalah konsep di mana Anda dapat menurunkan sifat dan perilaku dari kelas induk (Parent / Superclass) ke kelas anaknya (Child / Subclass). Dengan menggunakan pewarisan, Anda dapat membuat hierarki kelas di mana kelas anak mewarisi semua anggota (metode dan properti) dari kelas induk dan dapat menambahkan anggota tambahan yang spesifik untuk kelas anak tersebut. Misalnya, Anda dapat memiliki kelas "Karyawan" sebagai kelas induk, dan kelas "Manajer" sebagai kelas anak yang mewarisi sifat dan perilaku karyawan, tetapi juga memiliki metode tambahan seperti "kelolaTim" yang khusus untuk manajer.

- Polimorfisme (Polymorphism): Polimorfisme adalah konsep di mana objek dari kelas yang berbeda dapat memiliki perilaku yang berbeda untuk metode yang sama yang diwarisi dari kelas induk. Dalam polimorfisme, Anda dapat menggunakan objek dari kelas anak sebagai objek kelas induk, dan pemanggilan metode akan memanggil implementasi metode yang sesuai dengan kelas objek yang sebenarnya saat itu. Misalnya, jika Anda memiliki kelas "Hewan" dan kelas "Anjing" serta "Kucing" yang mewarisi dari kelas "Hewan", Anda dapat memiliki metode "bersuara" dalam kelas "Hewan". Ketika Anda memanggil metode "bersuara" pada objek "Anjing" dan "Kucing", masing-masing objek akan mengeluarkan suara yang sesuai dengan jenis hewan tersebut.





# No.4 
Mampu mendemonstrasikan penggunaan Encapsulation secara tepat (Lampirkan link source code terkait)
Jawab:
Encapsulation adalah salah satu konsep dalam pemrograman berorientasi objek (OOP) yang menggabungkan data dan fungsi yang beroperasi pada data tersebut dalam sebuah kelas. Tujuannya adalah untuk menyembunyikan detail implementasi dan membatasi akses langsung ke data internal kelas tersebut.

Dalam kode yang diberikan:

Private:
```
private String[] namaPenumpang;
private String[] tujuan;
private String[] kelas;
private int[] jumlahTiket;
private double[] hargaTiket;
private double[] totalHarga;
Variabel-variabel di atas dideklarasikan sebagai private. Ini berarti variabel-variabel tersebut hanya dapat diakses langsung dari dalam kelas TiketKereta tersebut. Variabel private tidak dapat diakses atau diubah nilainya secara langsung dari kelas lain. Hal ini membantu menerapkan prinsip encapsulation dengan menyembunyikan data internal kelas dan mencegah akses yang tidak sah atau tidak terkontrol.
```


Public:
```
public TiketKereta() {
        this.namaPenumpang = new String[10];
        this.tujuan = new String[10];
        this.kelas = new String[10];
        this.jumlahTiket = new int[10];
        this.hargaTiket = new double[10];
        this.totalHarga = new double[10];
    }    
```

Metode konstruktor TiketKereta dideklarasikan sebagai public. Metode ini dapat diakses dari luar kelas TiketKereta dan digunakan untuk membuat objek baru dari kelas tersebut. Dengan deklarasi public, metode konstruktor dapat diakses dan digunakan oleh kelas lain untuk membuat objek TiketKereta.

Penggunaan private dan public dalam kode tersebut menggambarkan penerapan encapsulation dalam OOP. Variabel-variabel private memungkinkan kontrol akses yang ketat terhadap data internal kelas, sementara metode konstruktor public memungkinkan pembuatan objek kelas dari luar. Dengan demikian, encapsulation membantu dalam menjaga privasi data dan mengatur akses terhadap kelas tersebut

# No.5
Mampu mendemonstrasikan penggunaan Abstraction secara tepat (Lampirkan link source code terkait)
Jawab:
```java
public abstract class AbstrakTiketKereta {
    public abstract void tambahkanTiket();

    public abstract void cekPemesanan();

    public abstract void pembatalan();

    public abstract void ubahJadwal();

    public abstract void jadwalBerangkat();

}
```
1. public abstract class AbstrakTiketKereta: Ini adalah deklarasi kelas abstrak yang bernama AbstrakTiketKereta. Kata kunci abstract menunjukkan bahwa kelas ini tidak dapat diinstansiasi langsung, tetapi hanya dapat diwarisi oleh kelas turunannya. Kelas ini berfungsi sebagai kerangka dasar untuk objek tiket kereta.
1. public abstract void tambahkanTiket();: Ini adalah deklarasi metode abstrak tambahkanTiket(). Metode ini tidak memiliki implementasi (tanpa blok kode di dalamnya) karena kelasnya adalah kelas abstrak. Metode ini harus diimplementasikan di kelas turunan yang mewarisi AbstrakTiketKereta.
1. public abstract void cekPemesanan();: Ini adalah deklarasi metode abstrak cekPemesanan(). Sama seperti metode sebelumnya, metode ini tidak memiliki implementasi dan harus diimplementasikan di kelas turunan.
1. public abstract void pembatalan();: Ini adalah deklarasi metode abstrak pembatalan(). Metode ini juga tidak memiliki implementasi dan harus diimplementasikan di kelas turunan.
1. public abstract void ubahJadwal();: Ini adalah deklarasi metode abstrak ubahJadwal(). Metode ini tidak memiliki implementasi dan harus diimplementasikan di kelas turunan.
1. public abstract void jadwalBerangkat();: Ini adalah deklarasi metode abstrak jadwalBerangkat(). Metode ini juga tidak memiliki implementasi dan harus diimplementasikan di kelas turunan.


Kelas abstrak seperti ini menyediakan kerangka kerja atau kontrak bagi kelas-kelas turunan untuk mengimplementasikan metode-metode abstrak ini sesuai dengan kebutuhan spesifik masing-masing kelas turunan. Dengan menggunakan konsep pewarisan, kelas turunan akan mewarisi metode-metode ini dan memberikan implementasi yang sesuai.



# No.6 
Mampu mendemonstrasikan penggunaan Inheritance dan Polymorphism secara tepat (Lampirkan link source code terkait)
Jawab:
```

import java.time.LocalTime;
import java.util.Random;
import java.util.Scanner;

public class TiketKereta extends AbstrakTiketKereta {
    private String[] namaPenumpang;
    private String[] tujuan;
    private String[] kelas;
    private int[] jumlahTiket;
    private double[] hargaTiket;
    private double[] totalHarga;

    public TiketKereta() {
        this.namaPenumpang = new String[10];
        this.tujuan = new String[10];
        this.kelas = new String[10];
        this.jumlahTiket = new int[10];
        this.hargaTiket = new double[10];
        this.totalHarga = new double[10];
    }

    @Override
    public void tambahkanTiket() {
        Scanner input = new Scanner(System.in);
        int i = 0;
        // Cari indeks kosong pada array
        while (i < namaPenumpang.length && namaPenumpang[i] != null) {
            i++;
        }

        if (i == namaPenumpang.length) {
            System.out.println("Maaf, pemesanan sudah penuh.");
            return;
        }

        System.out.print("Masukkan nama penumpang: ");
        namaPenumpang[i] = input.nextLine();

        System.out.print("Masukkan tujuan: ");
        tujuan[i] = input.nextLine();

        System.out.print("Masukkan kelas (ekonomi/bisnis/executive): ");
        kelas[i] = input.nextLine();

        System.out.print("Masukkan jumlah tiket: ");
        jumlahTiket[i] = input.nextInt();

        // Harga tiket sesuai kelas dan tujuan
        if (tujuan[i].equalsIgnoreCase("jakarta")) {
            if (kelas[i].equalsIgnoreCase("ekonomi")) {
                hargaTiket[i] = 75000;
            } else if (kelas[i].equalsIgnoreCase("bisnis")) {
                hargaTiket[i] = 150000;
            } else {
                hargaTiket[i] = 250000;
            }
        } else {
            if (kelas[i].equalsIgnoreCase("ekonomi")) {
                hargaTiket[i] = 50000;
            } else if (kelas[i].equalsIgnoreCase("bisnis")) {
                hargaTiket[i] = 100000;
            } else {
                hargaTiket[i] = 200000;
            }
        }
        totalHarga[i] = jumlahTiket[i] * hargaTiket[i];

        System.out.println("Tiket berhasil ditambahkan!");
    }

    @Override
    public void cekPemesanan() {
        System.out.println("==== DAFTAR PEMESANAN ====");
        System.out.println("No.  Nama Penumpang  Tujuan  Kelas  Jumlah Tiket  Harga Tiket  Total Harga");

        for (int i = 0; i < namaPenumpang.length; i++) {
            if (namaPenumpang[i] != null) {
                System.out.printf("%-5d%-16s%-8s%-7s%-13dRp%-11.2fRp%-12.2f\n", i + 1, namaPenumpang[i], tujuan[i],
                        kelas[i], jumlahTiket[i], hargaTiket[i], totalHarga[i]);
            }
        }
    }

    @Override
    public void pembatalan() {
        Scanner input = new Scanner(System.in);
            System.out.print("Masukkan nomor pemesanan yang akan dibatalkan: ");
            int nomor = input.nextInt();

            if (namaPenumpang[nomor - 1] == null) {
                System.out.println("Nomor pemesanan tidak valid.");
                return;
            }

            System.out.println("Apakah Anda yakin ingin membatalkan pemesanan ini? (y/n)");
            String konfirmasi = input.next();

            if (konfirmasi.equalsIgnoreCase("y")) {
                namaPenumpang[nomor - 1] = null;
                tujuan[nomor - 1] = null;
                kelas[nomor - 1] = null;
                jumlahTiket[nomor - 1] = 0;
                hargaTiket[nomor - 1] = 0;
                totalHarga[nomor - 1] = 0;
                System.out.println("Pemesanan berhasil dibatalkan.");
            }
    }

    @Override
    public void ubahJadwal() {
        Scanner input = new Scanner(System.in);
            System.out.print("Masukkan nomor pemesanan yang akan diubah: ");
            int nomor = input.nextInt();

            if (namaPenumpang[nomor - 1] == null) {
                System.out.println("Nomor pemesanan tidak valid.");
                return;
            }

            System.out.print("Masukkan tujuan baru: ");
            tujuan[nomor - 1] = input.next();

            System.out.print("Masukkan kelas baru (ekonomi/bisnis/executive): ");
            kelas[nomor - 1] = input.next();

            System.out.print("Masukkan jumlah tiket baru: ");
            jumlahTiket[nomor - 1] = input.nextInt();

            // Harga tiket sesuai kelas dan tujuan
            if (tujuan[nomor - 1].equalsIgnoreCase("jakarta")) {
                if (kelas[nomor - 1].equalsIgnoreCase("ekonomi")) {
                    hargaTiket[nomor - 1] = 75000;
                } else if (kelas[nomor - 1].equalsIgnoreCase("bisnis")) {
                    hargaTiket[nomor - 1] = 150000;
                } else if (kelas[nomor - 1].equalsIgnoreCase("executive")) {
                    hargaTiket[nomor - 1] = 250000;
                }
            } else if (tujuan[nomor - 1].equalsIgnoreCase("surabaya")) {
                if (kelas[nomor - 1].equalsIgnoreCase("ekonomi")) {
                    hargaTiket[nomor - 1] = 100000;
                } else if (kelas[nomor - 1].equalsIgnoreCase("bisnis")
                        || kelas[nomor - 1].equalsIgnoreCase("executive")) {
                    hargaTiket[nomor - 1] = 200000;
                }
            }
            totalHarga[nomor - 1] = jumlahTiket[nomor - 1] * hargaTiket[nomor - 1];

        System.out.println("Jadwal berhasil diubah!");
    }

    @Override
    public void jadwalBerangkat() {
        String[] namaKereta = { "Argo Bromo Anggrek", "Gajayana", "Argo Wilis", "Mutiara Selatan" };
        String[] stasiunAsal = { "Jakarta", "Surabaya", "Malang", "Yogyakarta" };
        String[] stasiunTujuan = { "Surabaya", "Jakarta", "Bandung", "Semarang" };
        Random random = new Random();

        for (int i = 0; i < 3; i++) {
            int indexNama = random.nextInt(namaKereta.length);
            int indexAsal = random.nextInt(stasiunAsal.length);
            int indexTujuan = random.nextInt(stasiunTujuan.length);
            LocalTime jamBerangkat = LocalTime.of(random.nextInt(24), random.nextInt(60));
            LocalTime jamTiba = jamBerangkat.plusHours(random.nextInt(5) + 1);

            JadwalBerangkat jadwal = new JadwalBerangkat(namaKereta[indexNama], stasiunAsal[indexAsal],
                    stasiunTujuan[indexTujuan], jamBerangkat, jamTiba);
            jadwal.tampilkanJadwal();
            System.out.println();
        }
    }
}

```
Kode di atas adalah implementasi kelas TiketKereta yang merupakan turunan dari kelas abstrak AbstrakTiketKereta. Kode tersebut mencakup contoh penggunaan konsep Inheritance dan Polymorphism. Berikut adalah penjelasan mengenai kedua konsep tersebut dalam konteks kode tersebut:

- Inheritance: Konsep inheritance (pewarisan) digunakan di sini dengan meng-extend kelas TiketKereta dari kelas abstrak AbstrakTiketKereta. Dengan mewarisi kelas abstrak tersebut, kelas TiketKereta mendapatkan semua metode abstrak yang didefinisikan di dalam kelas abstrak tersebut, yaitu tambahkanTiket(), cekPemesanan(), pembatalan(), ubahJadwal(), dan jadwalBerangkat(). Dalam kelas TiketKereta, kita memberikan implementasi konkret untuk setiap metode abstrak tersebut.

- Polymorphism: Konsep polymorphism (polimorfisme) digunakan di sini dalam bentuk overriding. Metode-metode yang diwarisi dari kelas abstrak AbstrakTiketKereta di-override (ditimpa) dengan implementasi yang sesuai di kelas TiketKereta. Ini memungkinkan kita untuk mengganti perilaku metode-metode tersebut sesuai dengan kebutuhan spesifik kelas TiketKereta. Misalnya, dalam metode tambahkanTiket(), kita mengganti logika untuk menambahkan tiket kereta dengan menerima masukan dari pengguna melalui konsol. Sedangkan dalam metode cekPemesanan(), kita menampilkan daftar pemesanan tiket kereta yang telah ditambahkan sebelumnya.

Dengan menggunakan inheritance dan polymorphism, kita dapat memanfaatkan kerangka kerja yang telah didefinisikan dalam kelas abstrak AbstrakTiketKereta dan memberikan implementasi yang spesifik untuk kelas TiketKereta. Hal ini memungkinkan kita untuk mengatur dan mengelola pemesanan tiket kereta dengan menggunakan objek dari kelas TiketKereta, sambil memanfaatkan fitur-fitur yang telah didefinisikan di kelas abstrak tersebut.


# No.7
Mampu menerjemahkan proses bisnis ke dalam skema OOP secara tepat. Bagaimana cara Kamu mendeskripsikan proses bisnis (kumpulan use case) ke dalam OOP ?


Jawab:

Menerjemahkan proses bisnis menjadi skema OOP melibatkan identifikasi entitas, atribut, dan perilaku yang terkait dengan proses tersebut. Berikut adalah cara saya akan mendeskripsikan proses bisnis ke dalam skema OOP:

1. Identifikasi entitas: Identifikasi entitas-entitas yang terlibat dalam proses bisnis. Misalnya, entitas seperti Pengguna, Tiket, JadwalKeberangkatan, Pembayaran, dan sebagainya.

2. Definisikan kelas: Setelah mengidentifikasi entitas, buat kelas-kelas yang mewakili entitas tersebut. Misalnya, buat kelas Pengguna, Tiket, JadwalKeberangkatan, dan Pembayaran.

3. Attribut: Tentukan atribut-atribut yang relevan untuk setiap kelas. Misalnya, kelas Pengguna mungkin memiliki atribut seperti username, password, dan email. Kelas Tiket mungkin memiliki atribut seperti nomorTiket, stasiunAsal, dan stasiunTujuan. Attribut-atribut ini memodelkan data yang terkait dengan entitas dalam proses bisnis.

4. Metode: Tentukan perilaku yang berkaitan dengan entitas tersebut dengan menentukan metode pada setiap kelas. Misalnya, kelas Pengguna mungkin memiliki metode register() untuk proses registrasi, dan kelas Tiket mungkin memiliki metode pesanTiket() untuk melakukan pemesanan tiket.

5. Hubungan antar kelas: Identifikasi dan definisikan hubungan antara kelas-kelas. Misalnya, kelas PemesananTiket memiliki komposisi dengan kelas Tiket dan JadwalKeberangkatan, karena pemesanan tiket terkait dengan tiket dan jadwal keberangkatan yang spesifik.

6. Penggunaan inheritance: Jika terdapat hierarki entitas yang terkait, pertimbangkan penggunaan inheritance untuk menggambarkan hierarki tersebut. Misalnya, jika terdapat jenis tiket yang berbeda, Anda dapat menggunakan inheritance untuk membuat kelas induk Tiket dan kelas anak seperti TiketBiasa dan TiketPremium.

7. Implementasi metode: Implementasikan metode-metode yang telah ditentukan dalam kelas-kelas yang sesuai. Metode-metode ini akan menjalankan logika bisnis yang sesuai dengan proses yang ingin dicapai.

8. Penggunaan objek: Dalam penggunaan skema OOP, Anda akan membuat objek-objek dari kelas-kelas yang telah Anda definisikan untuk mewakili entitas dalam proses bisnis. Objek-objek ini akan berinteraksi satu sama lain untuk mencapai tujuan bisnis yang diinginkan.

9. Tes dan iterasi: Setelah menerjemahkan proses bisnis ke dalam skema OOP, lakukan pengujian dan iterasi untuk memastikan bahwa logika bisnis dan hubungan antar kelas bekerja sesuai harapan. Lakukan perbaikan dan perubahan jika diperlukan.



# No.8
Mampu menjelaskan rancangan dalam bentuk Class Diagram, dan Use Case table (Lampirkan diagram terkait)

Jawab:

**USE CASE DIAGRAM**

![Use Case Tiket](ss/1. use case tiket.png)


**USE CASE DIAGRAM**

![ ](ss/2. diagram class.png)


Berikut ini adalah beberapa skenario untuk use case yang terdapat dalam diagram UML:

1. **Skenario Pencarian Jadwal:**
   - Pengguna mengakses fitur "Pencarian Jadwal" pada aplikasi Kereta Access.
   - Sistem menampilkan formulir pencarian jadwal.
   - Pengguna memasukkan kriteria pencarian seperti stasiun asal, stasiun tujuan, dan tanggal keberangkatan.
   - Sistem memproses permintaan dan menampilkan jadwal kereta yang sesuai dengan kriteria pencarian.

2. **Skenario Pemesanan Tiket:**
   - Pengguna mengakses fitur "Pemesanan Tiket" pada aplikasi Kereta Access.
   - Sistem menampilkan halaman pemilihan jadwal.
   - Pengguna memilih jadwal kereta yang diinginkan.
   - Sistem memproses pilihan pengguna dan menampilkan halaman pemilihan kursi.
   - Pengguna memilih kursi yang tersedia.
   - Sistem memvalidasi dan mengumpulkan informasi pengguna, jadwal, dan kursi yang dipilih.
   - Pengguna mengonfirmasi pesanan tiket.
   - Sistem memproses pembayaran dan mengeluarkan tiket.

3. **Skenario Pembatalan Tiket:**
   - Pengguna mengakses fitur "Pembatalan Tiket" pada aplikasi Kereta Access.
   - Sistem menampilkan halaman riwayat pemesanan tiket pengguna.
   - Pengguna memilih tiket yang ingin dibatalkan.
   - Sistem memvalidasi permintaan pembatalan tiket.
   - Sistem memproses pembatalan dan mengembalikan dana jika berlaku.

4. **Skenario Lihat Riwayat:**
   - Pengguna mengakses fitur "Lihat Riwayat" pada aplikasi Kereta Access.
   - Sistem menampilkan halaman riwayat pemesanan tiket pengguna.
   - Pengguna melihat daftar riwayat pemesanan tiket sebelumnya.

5. **Skenario Kelola Jadwal:**
   - Administrator mengakses fitur "Kelola Jadwal" pada aplikasi Kereta Access.
   - Sistem menampilkan halaman pengelolaan jadwal.
   - Administrator dapat menambah, mengedit, atau menghapus jadwal kereta sesuai kebutuhan.

6. **Skenario Kelola Pengguna:**
   - Administrator mengakses fitur "Kelola Pengguna" pada aplikasi Kereta Access.
   - Sistem menampilkan halaman pengelolaan pengguna.
   - Administrator dapat menambah, mengedit, atau menghapus pengguna aplikasi Kereta Access.

Skenario-skenario di atas memberikan gambaran singkat tentang bagaimana use case dapat digunakan dalam konteks sistem Kereta Access. Tentu saja, masih ada banyak kemungkinan skenario tambahan tergantung pada kebutuhan dan kompleksitas aplikasi tersebut.

**USE CASE PRIORITY**
| Use Case                     | Priority | Status   |
|------------------------------|----------|----------|
| Registrasi                   | Tinggi   | Sudah  |
| Login                        | Tinggi   | Sudah  |
| Pemesanan Tiket              | Tinggi   | Sudah |
| Pembayaran Tiket             | Tinggi   | Sudah |
| Pembatalan Pemesanan Tiket   | Tinggi   | Sudah |
| Cek Pemesanan Tiket          | Tinggi   | Sudah |
| Cek Jadwal Keberangkatan     | Sedang   | Sudah |
| Pencarian Jadwal Keberangkatan | Sedang | Sudah |
| Pembatalan Tiket             | Tinggi   | Sudah |




# No.9
Mampu memberikan gambaran umum aplikasi kepada publik menggunakan presentasi berbasis video (Lampirkan link Youtube terkait)
Jawab:

![Demo Program Kereta Access](https://youtu.be/Nt9B0wKca7E)

# No.10
Inovasi UX (Lampirkan url screenshot aplikasi di Gitlab / Github)
Jawab:
![](ss/1.png)
![](ss/2.png)
![](ss/3.png)
![](ss/4.png)
![](ss/5.png)
![](ss/6.png)
![](ss/7.png)
![](ss/8.png)
![](ss/9.png)
![](ss/10.png)
![](ss/11.png)
![](ss/12.png)
![](ss/13.png)
![](ss/14.png)





